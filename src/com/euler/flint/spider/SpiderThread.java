/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.flint.spider;

import javax.persistence.EntityManager;

/**
 *
 * @author root
 */
public class SpiderThread implements Runnable {

    private int start, end;
    private EntityManager em = null;

    public SpiderThread(int start, int end, EntityManager em) {
        this.start = start;
        this.end = end;
        this.em = em;
    }

    @Override
    public void run() {
        try {
            for (int i = start; i < end; i++) {
                // Pause for 4 seconds
                Thread.sleep(150);
                // Print a message
                //processPage(i);
            }
        } catch (Exception e) {
            threadMessage("I wasn't done!");
            threadMessage("------------EXCEPTION: " + e.getLocalizedMessage());
        }
    }

    // Display a message, preceded by
    // the name of the current thread
    private static void threadMessage(String message) {
        String threadName = Thread.currentThread().getName();
        System.out.format("%s: %s%n", threadName, message);
    }

    /**
     * @param <T>
     * @param entity
     * @return
     */
    public <T> T persist(T entity) {
        try {
            if (!em.getTransaction().isActive()) {
                em.getTransaction().begin();
            }
            em.persist(entity);
            em.getTransaction().commit();
            return entity;
        } catch (Exception exc) {
            threadMessage("Exception found!" + exc.getLocalizedMessage());
        }

        return entity;

    }

    /**
     *
     * @param <T>
     * @param entity
     * @return
     */
    public <T> T merge(T entity) {
        try {
            if (!em.getTransaction().isActive()) {
                em.getTransaction().begin();
            }            
            em.merge(entity);
            em.getTransaction().commit();
            return entity;

        } catch (Exception exc) {
            threadMessage("Exception found!" + exc.getLocalizedMessage());
        }
        return entity;
    }
}