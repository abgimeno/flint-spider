/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.flint.spider.model.workout.group;

import com.euler.flint.spider.model.workout.ExerciseType;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author agime
 */
@Entity
public class GroupActivity extends ExerciseType implements Serializable {

    @Override
    public String toString() {
        return "com.euler.flint.spider.model.workout.group.GroupActivity[ id=" + super.getId() + " ]";
    }
    
}
