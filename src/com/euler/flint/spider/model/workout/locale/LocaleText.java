/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.flint.spider.model.workout.locale;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import javax.persistence.TableGenerator;


/**
 *
 * @author root
 */

@MappedSuperclass
@Inheritance(strategy = InheritanceType.JOINED)
public class LocaleText implements Serializable{

    public static final String QUERY_GETBYNAME="LocaleText.getByName";
    public static final String PARAM_NAME="name";
    
    
    @Id
    @TableGenerator(name = "localeGen", table = "EJB_ORDER_SEQUENCE_GENERATOR", pkColumnName = "GEN_KEY", valueColumnName = "GEN_VALUE", pkColumnValue = "localeId", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "localeGen")    
    private Long id;
    
    @OneToOne
    @JoinColumn(name="FK_LANGUAGE_ID")
    private Language language;
   
    @Column(name="TEXT", length=20000)
    private String text;

    public LocaleText() {
    }

    public LocaleText( String text, Language language) {
        this.language = language;
        this.text = text;
    }
    
    public LocaleText(String text) {
        this.text=text;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Language getLanguageDescriptor() {
        return language;
    }

    public void setLanguageDescriptor(Language languageDescriptor) {
        this.language = languageDescriptor;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }


}
