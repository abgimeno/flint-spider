/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.flint.spider.model.workout.weight;

import com.euler.flint.spider.model.workout.ExerciseType;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author agime
 */
@Entity
public class WeightExercise extends ExerciseType implements Serializable  {

    @Column
    private int nsets;
    
    @Column
    private int nrep;
    

    public int getNsets() {
        return nsets;
    }

    public void setNsets(int nsets) {
        this.nsets = nsets;
    }

    public int getNrep() {
        return nrep;
    }

    public void setNrep(int nrep) {
        this.nrep = nrep;
    }
   
    
    @Override
    public String toString() {
        return "com.euler.flint.spider.model.workout.weight.Weight[ id=" + super.getId() + " ]";
    }
    
}
