/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.flint.spider.model.workout.weight;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.TableGenerator;

/**
 *
 * @author root
 */
@Entity
public class MediaUrl implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id    
    @TableGenerator(name = "mediaUrlGen", table = "EJB_ORDER_SEQUENCE_GENERATOR", 
                  pkColumnName = "GEN_KEY", valueColumnName = "GEN_VALUE", 
                  pkColumnValue = "mediaUrlId",initialValue=1, allocationSize = 1 )     
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "mediaUrlGen")   
    private Long id;
    
    @Column
    private String url;
    
    @ManyToOne
    private Movement movement;

    public MediaUrl() {
    }

    public MediaUrl(String url, Movement movement) {
        this.url = url;
        this.movement = movement;
    }

    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MediaUrl)) {
            return false;
        }
        MediaUrl other = (MediaUrl) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.euler.flint.spider.model.workout.Photo[ id=" + id + " ]";
    }
    
}
