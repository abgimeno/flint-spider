/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.flint.spider.extractor;

import com.euler.flint.spider.model.workout.locale.Language;
import javax.persistence.EntityManager;

/**
 *
 * @author root
 */
public class BaseExtractor {
    
    protected static EntityManager em;
    

     // Display a message, preceded by
    // the name of the current thread
    protected static void threadMessage(String message) {
        String threadName = Thread.currentThread().getName();
        System.out.format("%s: %s%n", threadName, message);
    }
    
    protected EntityManager getEntityManager(){
        return BaseExtractor.em;
    }
    
    protected static Language getEnglish() {
        try {
            if (!em.getTransaction().isActive()) {
                em.getTransaction().begin();
            }
            return em.find(Language.class, Language.LANGUAGE_ID_ENGLISH);
            //em.getTransaction().commit();
        } catch (Exception exc) {
            threadMessage("Exception found!----------" + exc.getLocalizedMessage());
        }
        return null;
    }

    /**
     * @param <T>
     * @param entity
     * @return
     */
    protected static <T> T persist(T entity) {
        try {
            if (!em.getTransaction().isActive()) {
                em.getTransaction().begin();
            }
            em.persist(entity);
            em.getTransaction().commit();
            return entity;
        } catch (Exception exc) {
            threadMessage("Exception found!----------" + exc.getLocalizedMessage());
        }

        return entity;

    }

    /**
     *
     * @param <T>
     * @param entity
     * @return
     */
    protected static <T> T merge(T entity) {
        try {
            if (!em.getTransaction().isActive()) {
                em.getTransaction().begin();
            }
            em.merge(entity);
            em.getTransaction().commit();
            return entity;

        } catch (Exception exc) {
            threadMessage("Exception found!----------" + exc.getLocalizedMessage());
        }
        return entity;
    }
    
    protected static void setEntityManager(EntityManager em){
        BaseExtractor.em=em;  
    }


}
