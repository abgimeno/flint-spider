/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.flint.spider.extractor;

import com.euler.flint.spider.NoValueFoundException;
import com.euler.flint.spider.model.data.LocalUrl;
import com.euler.flint.spider.model.workout.locale.Description;
import com.euler.flint.spider.model.workout.weight.Movement;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author root
 */
public class DescriptionExtractor extends BaseExtractor {

    public static List<Description> extractDescription(EntityManager em, Movement move, Document site) throws NoValueFoundException {
        setEntityManager(em);
        List<Description> descs = new ArrayList<Description>();
        Elements ths = site.select("td");
        for (Element th : ths) {
            String thTag = th.attr("class");
            if (thTag.equals("textPadding") && !thTag.isEmpty()) { //finds the class that contains the text we want to extract
                String thText = th.text();
                threadMessage("**" + thTag + "**" + thText);
                if (thText.isEmpty()) {
                    throw new NoValueFoundException();
                }
                descs.add(persist(new Description(thText, getEnglish(), move)));
            }
        }

        return descs;

    }
}
