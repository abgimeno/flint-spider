/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.flint.spider.extractor.relatedinfo;

import com.euler.flint.spider.extractor.relatedinfo.RelatedInfoExtractor;
import com.euler.flint.spider.model.workout.weight.Movement;
import com.euler.flint.spider.model.workout.weight.MuscleGroup;
import java.util.List;
import javax.persistence.EntityManager;
import org.jsoup.nodes.Document;

/**
 *
 * @author root
 */
public class MuscleGroupExtractor extends RelatedInfoExtractor {


    public static List<MuscleGroup> extractMuscleGroup(EntityManager em, Movement move, Document site) {
        setEntityManager(em);
        return (List<MuscleGroup>) extractAllRelatedInfo("Main Muscle Group :", move, site);

    }    
   
}
