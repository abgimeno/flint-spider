/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.flint.spider.extractor.relatedinfo;

import com.euler.flint.spider.model.workout.weight.Equipment;
import com.euler.flint.spider.model.workout.weight.Movement;
import java.util.List;
import javax.persistence.EntityManager;
import org.jsoup.nodes.Document;

/**
 *
 * @author root
 */
public class EquipmentExtractor extends RelatedInfoExtractor {


    public static List<Equipment> extractEquipment(EntityManager em, Movement move, Document site) {
        setEntityManager(em);
        return (List<Equipment>) extractAllRelatedInfo("Equipment : ", move, site);

    }

    
}
