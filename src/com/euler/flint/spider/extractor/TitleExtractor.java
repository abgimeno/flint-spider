/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.flint.spider.extractor;

import com.euler.flint.spider.NoValueFoundException;
import com.euler.flint.spider.model.workout.locale.Language;
import com.euler.flint.spider.model.workout.locale.Title;
import com.euler.flint.spider.model.workout.weight.Movement;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/**
 *
 * @author root
 */
public class TitleExtractor extends BaseExtractor {

    public static List<Title> extractTitle(EntityManager em, Movement move, Document site) throws NoValueFoundException {

        setEntityManager(em);
        List<Title> titles = new ArrayList<Title>();
        Title t = null;
        if (!em.getTransaction().isActive()) {
            em.getTransaction().begin();
        }
        Elements ths = site.select("thead");
        if (ths.isEmpty() || ths.get(0).text().isEmpty()) {
            throw new NoValueFoundException();
        }
        Language english = em.find(Language.class, Language.LANGUAGE_ID_ENGLISH);
        t = new Title(ths.get(0).text(), english, move);
        em.persist(t);
        em.getTransaction().commit();
        titles.add(t);

        return titles;

    }
}
