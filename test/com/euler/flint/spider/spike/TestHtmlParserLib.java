/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.flint.spider.spike;

import com.euler.flint.spider.test.TestExtractMovementTitle;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author root
 */
public class TestHtmlParserLib {

    public TestHtmlParserLib() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    //

    @Test
    public void extractFromFile() {
        try {
            File web = new File("/jetfit-pages/AlternateBentOverDumbbellKickbackExercise.html");

            Document site = null;
            site = Jsoup.connect("http://localhost:8080/SupineTricepsExtensiononExerciseBallExercise.html").userAgent("Mozilla")
                    .timeout(3000)
                    .get();

            Elements links = site.select("a[href]");
            Elements media = site.select("[src]");
            Elements imports = site.select("link[href]");
            Elements ths = site.select("td");
            Elements ps =  site.select("p");

            print("\n Ps: (%d)", ps.size());

            for (Element p : ps) {
                String thTag = p.attr("class");
                String thText = p.text();
                System.out.println("**" + thTag + "**" + thText);
            }
            
            print("\nTable Header: (%d)", ths.size());

            for (Element th : ths) {
                String thTag = th.attr("class");
                if(thTag.equals("textPadding")){
                    String thText = th.text();
                    System.out.println("**" + thTag + "**" + thText);
                }
               

            }
            print("\nLinks: (%d)", links.size());

            for (Element link : links) {
                String linkHref = link.attr("href");
                String linkText = link.text();
                System.out.println("**" + linkHref + "**" + linkText);

            }
            print("\nMedia: (%d)", media.size());
            for (Element src : media) {
                if (src.tagName().equals("img")) {
                    print(" * %s: <%s> %sx%s (%s)",
                            src.tagName(), src.attr("abs:src"), src.attr("width"), src.attr("height"),
                            trim(src.attr("alt"), 20));

                } else {
                    print(" * %s: <%s>", src.tagName(), src.attr("abs:src"));

                }
            }

            print("\nImports: (%d)", imports.size());
            for (Element link : imports) {
                print(" * %s <%s> (%s)", link.tagName(), link.attr("abs:href"), link.attr("rel"));
            }

            print("\nLinks: (%d)", links.size());
            for (Element link : links) {
                print(" * a: <%s>  (%s)", link.attr("abs:href"), trim(link.text(), 35));
            }

        } catch (IOException ex) {
            Logger.getLogger(TestExtractMovementTitle.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private static void print(String msg, Object... args) {
        System.out.println(String.format(msg, args));
    }

    private static String trim(String s, int width) {
        if (s.length() > width) {
            return s.substring(0, width - 1) + ".";

        } else {
            return s;

        }
    }
}
