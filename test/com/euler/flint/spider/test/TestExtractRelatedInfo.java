/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.flint.spider.test;

import com.euler.flint.spider.model.data.DifficultyLevels;
import com.euler.flint.spider.model.data.Equipment;
import com.euler.flint.spider.model.data.LocalUrl;
import com.euler.flint.spider.model.data.MuscleGroupList;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author agime
 */
public class TestExtractRelatedInfo {

    private List<Elements> elements = new ArrayList<Elements>();

    public TestExtractRelatedInfo() throws IOException {
        Document site = null;
        for (String s : LocalUrl.getUrls()) {
            site = Jsoup.connect(s).userAgent("Mozilla").timeout(3000).get();
            elements.add(site.select("p"));
        }

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //

    @Test
    public void extractMuscleGroup() {
        extractRelatedInfo("Main Muscle Group :", MuscleGroupList.plainList);
    }

    @Test
    public void extractDifficulty() {
        extractRelatedInfo("Difficulty : ", DifficultyLevels.plainList);
    }

    @Test
    public void extractEquipment() {
        extractRelatedInfo("Equipment : ", Equipment.plainList);
    }

    private String extractRelatedInfo(String relatedInfo, String plainList) {
        String thText = null;
        for (Elements ps : elements) {
            for (Element p : ps) {
                if (p.text().contains(relatedInfo)) {
                    thText = p.text().replace(relatedInfo, "").trim();
                    if(thText.contains(",")){
                        List<String> values = extractIndividualValues(thText);
                        for (String s :values){
                            //System.out.println(s + " es " +plainList.contains(s));
                            assertEquals(true, plainList.contains(s));
                        }
                    }
                    else{
                        //System.out.println("En " +relatedInfo +" "+thText + " es " +plainList.contains(thText));
                        assertEquals(true, plainList.contains(thText));
                    }
                    //System.out.println("****" + thText);
                }                                
            }
        }
        return thText;
    }

    private List<String> extractIndividualValues(String thText) {
        List<String> values = new ArrayList<String>();
        
        while(thText.contains(",")){
            int i = thText.indexOf(",");
            //System.out.println("****" + thText.substring(0, i).trim()+"****" );
            values.add(thText.substring(0, i).trim());
            thText= thText.substring(i+1,thText.length()).trim();
            //System.out.println("**** Se queda con " + thText+"****" );
        }
        values.add(thText.trim());
        //System.out.println("**** El ultimo es " + thText+"****" );
        return values;
    }
}
