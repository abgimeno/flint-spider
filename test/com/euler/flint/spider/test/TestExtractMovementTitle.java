/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.flint.spider.test;

import com.euler.flint.spider.NoValueFoundException;
import com.euler.flint.spider.model.data.LocalUrl;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.jsoup.nodes.Element;

/**
 *
 * @author root
 */
public class TestExtractMovementTitle {

    private static final List<String> names;
    private Document site = null;

    static {
        List<String> aMap = new ArrayList<String>();
        aMap.add("Alternate Bent Over Dumbbell Kickback");
        aMap.add("Supine Triceps Extension on Exercise Ball");
        aMap.add("Chest Dip");
        aMap.add("Alternate Lying Dumbbell Extension");
        aMap.add("Empty Page");

        names = Collections.unmodifiableList(aMap);
    }

    public TestExtractMovementTitle() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //

    @Test
    public void extractMovementTile() throws NoValueFoundException {
        try {
            for (int i = 0; i < LocalUrl.getUrls().size(); i++) {
                site = Jsoup.connect(LocalUrl.getUrls().get(i)).userAgent("Mozilla").timeout(3000).get();
                Elements ths = site.select("thead");
                if (ths.get(0).text().isEmpty()) {
                    throw new NoValueFoundException();
                }
                assertEquals(ths.get(0).text(), names.get(i));
            }

        } catch (IOException ex) {
            Logger.getLogger(TestExtractMovementTitle.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Test
    public void handleMissingTitle() throws NoValueFoundException, IOException {
        try {
            site = Jsoup.connect(LocalUrl.getEmtpyPage()).userAgent("Mozilla").timeout(3000).get();
            Elements ths = site.select("thead");
            if (ths.get(0).text().isEmpty()) {
                throw new NoValueFoundException();
            }
            //fail indicates that if we got this far then test is not correct
            fail("at this point an exception should have been thrown");
        } catch (NoValueFoundException nvf) {
            assertEquals("No value has been found for the selected HTML", nvf.getMessage());
        } catch (IOException ex) {
            Logger.getLogger(TestExtractMovementTitle.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
