/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.flint.spider.test;

import com.euler.flint.spider.NoValueFoundException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author agime
 */
public class TestExtractImageName {
    private static final List<String> urls;

    static {
        List<String> aMap = new ArrayList<String>();
        aMap.add("http://localhost:8080/AlternateBentOverDumbbellKickbackExercise_files/4640.jpg");
        aMap.add("http://localhost:8080/AlternateBentOverDumbbellKickbackExercise_files/4641.jpg");
        aMap.add("http://localhost:8080/SupineTricepsExtensiononExerciseBallExercise_files/3124.jpg");
        aMap.add("http://localhost:8080/SupineTricepsExtensiononExerciseBallExercise_files/3125.jpg");
        aMap.add("http://localhost:8080/ChestDipExercise_files/96.jpg");
        aMap.add("http://localhost:8080/ChestDipExercise_files/97.jpg");
        aMap.add("http://localhost:8080/AlternateLyingDumbbellExtensionExercise_files/4644.jpg");
        aMap.add("http://localhost:8080/AlternateLyingDumbbellExtensionExercise_files/4645.jpg");

        urls = Collections.unmodifiableList(aMap);
    }
    private static final List<String> names;

    static {
        List<String> aMap = new ArrayList<String>();
        aMap.add("4640.jpg");
        aMap.add("4641.jpg");
        aMap.add("3124.jpg");
        aMap.add("3125.jpg");
        aMap.add("96.jpg");
        aMap.add("97.jpg");
        aMap.add("4644.jpg");
        aMap.add("4645.jpg");

        names = Collections.unmodifiableList(aMap);
    }    
    public TestExtractImageName() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void extractNameFromUrl() throws NoValueFoundException {
        for (int i = 0; i<urls.size();i++){
            assertEquals(extractName(urls.get(i)),names.get(i));
        }
    
    }

    public static String extractName(String line) throws NoValueFoundException {
        //System.out.println(line);
        Matcher m = Pattern.compile("(http://)(.+)(/)(.+)(.jpg)").matcher(line);        
        if (m.matches()) {            
            //System.out.println("Found: "+m.group(4)+m.group(5));
            return m.group(4)+m.group(5);            
        } else {
            throw new NoValueFoundException();
        }
      
    }    
}
