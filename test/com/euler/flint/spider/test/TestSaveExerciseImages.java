/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.flint.spider.test;

import com.euler.flint.spider.NoValueFoundException;
import com.euler.flint.spider.model.data.LocalUrl;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author agime
 */
public class TestSaveExerciseImages {

    public TestSaveExerciseImages() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //

    @Test
    public void extractAndStoreExerciseImages() {
        try {

            Document site = null;
            for (String s : LocalUrl.getUrls()) {
                site = Jsoup.connect(s).userAgent("Mozilla").timeout(3000).get();
                Elements media = site.select("[src]");
                for (Element src : media) {
                    if (src.tagName().equals("img") && src.attr("width").equals("275") && src.attr("height").equals("197")) {
                        System.out.println("Link: " + src.attr("abs:src"));
                        downloadAndSave(TestExtractImageName.extractName(src.attr("abs:src")), src.attr("alt"), src.attr("abs:src"));
                    }
                }
            }

        } catch (NoValueFoundException ex) {
            Logger.getLogger(TestSaveExerciseImages.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            System.out.printf(TestExtractMovementTitle.class.getName(), ex);
        }

    }

    private void downloadAndSave(String name, String dir, String urlString) {
        InputStream in = null;
        try {
            URL url = new URL(urlString);
            in = new BufferedInputStream(url.openStream());
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            int n = 0;
            while (-1 != (n = in.read(buf))) {
                out.write(buf, 0, n);
            }
            out.close();
            in.close();
            byte[] response = out.toByteArray();

            FileOutputStream fos = new FileOutputStream(name);
            fos.write(response);
            fos.close();
        } catch (IOException ex) {
            Logger.getLogger(TestSaveExerciseImages.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                in.close();
            } catch (IOException ex) {
                Logger.getLogger(TestSaveExerciseImages.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }


}
