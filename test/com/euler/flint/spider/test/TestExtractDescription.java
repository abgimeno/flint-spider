/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.euler.flint.spider.test;

import com.euler.flint.spider.NoValueFoundException;
import com.euler.flint.spider.model.data.LocalUrl;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author root
 */
public class TestExtractDescription {

    private static final List<String> descs;
    private Document site = null;

    static {
        List<String> aMap = new ArrayList<String>();
        aMap.add("Steps : 1.) Start off standing up straight with your feet shoulder width apart, holding a dumbbell in each hand. 2.) Bend your back down towards the floor so that it is parallel to the ground, bring the dumbbells to your sides and tuck your upper arms towards your body. 3.) Slowly raise one dumbbell up behind you until your arm is fully extended and squeeze your tricep. 4.) Hold for a count then return back to the starting position. 5.) Repeat for as many reps and sets as desired.");
        aMap.add("Steps : 1.) Start off by laying on your back on an exercise ball with the top of the ball underneath your upper back, your feet firmly on the floor and holding a dumbbell in one hand. 2.) Slowly raise your arm with the dumbbell straight up towards the ceiling and keeping the other arm at your side. 3.) Bend your arm and bring the weight slowly down behind your head feeling a stretch in your tricep and hold for a count. 4.) Then bring the weight back up to the starting position and repeat for as many reps and sets desired.");
        aMap.add("The chest dip exercise is a variation of the tricep dip exercise that is mainly focused upon your chest rather than the tricep muscle. Steps : 1.) Start off by gripping the bars of a dip bar (if you do not know what this machine is ask for assistance) and push yourself up into the starting position. 2.) In the starting position of this exercise your arms should be kept straight and close to your body and knees bent so that it never touches the ground. 3.) Performing this exercise, slowly lower your body towards the ground, leading with your chest on the descent. 4.) As you lower your body, your chest should be pointing down towards the ground at a 45 degree angle. 5.) Continue to lower your body until you feel a stretch in your chest and shoulders, hold for a count then return back to the starting position. Tips : 1.) If you cannot manage this exercise yet, it is important to get or use a spotter to hold onto your feet and assist you in the motion. 2.) Prevent from locking your elbows in the starting position and keep your abs tight throughout the exercise.");
        aMap.add("Steps : 1.) Start off laying down on flat bench with your feet on the floor in front of you, holding a dumbbell in each hand in a neutral grip elevated above your head. 2.) Slowly lower one of the dumbbells down towards your face so that it is at level with your ear and hold for a count. 3.) Return back to the starting position and repeat with the opposite arm. 4.) Repeat for as many reps and sets as desired.");
        aMap.add("Empty Page");

        descs = Collections.unmodifiableList(aMap);
    }
    public TestExtractDescription() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
    // TODO add test methods here.

    @Test
    public void extractDescription() throws NoValueFoundException {
        try {
            for (int i = 0; i < LocalUrl.getUrls().size(); i++) {
                site = Jsoup.connect(LocalUrl.getUrls().get(i)).userAgent("Mozilla").timeout(3000).get();
                Elements ths = site.select("td");
                for (Element th : ths) {
                    String thTag = th.attr("class");
                    if (thTag.equals("textPadding") && !thTag.isEmpty()) {
                        String thText = th.text();
                        System.out.println("**" + thTag + "**" + thText);
                        if (thText.isEmpty()) {
                            throw new NoValueFoundException();
                        }
                        assertEquals(thText,descs.get(i));
                    } 
                }
            }

        } catch (IOException ex) {
            System.out.printf(TestExtractMovementTitle.class.getName(), ex);
        }

    }

    @Test
    public void handleMissingDescription() throws NoValueFoundException, IOException {
        try {
            site = Jsoup.connect(LocalUrl.getEmtpyPage()).userAgent("Mozilla").timeout(3000).get();
            Elements ths = site.select("td");
            for (Element th : ths) {
                String thTag = th.attr("class");
                if (thTag.equals("textPadding") && !thTag.isEmpty()) {
                    String thText = th.text();
                    System.out.println("**" + thTag + "**" + thText);
                } else {
                    if (thTag.isEmpty()) {
                        throw new NoValueFoundException();
                    }
                }
            }
            //fail indicates that if we got this far then test is not correct
            fail("at this point an exception should have been thrown");
        } catch (NoValueFoundException nvf) {
            assertEquals("No value has been found for the selected HTML", nvf.getMessage());
        } catch (IOException ex) {
            Logger.getLogger(TestExtractMovementTitle.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
